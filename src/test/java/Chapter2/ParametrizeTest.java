package Chapter2;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class ParametrizeTest {
    private String fileName;

    public ParametrizeTest(String fileName) {
        this.fileName = fileName;
    }

    @Parameterized.Parameters(name = "{index}: {0}")
    public static Iterable<Object[]> dataForTest() {
        return Arrays.asList(new Object[][]{
                {"filewithgoodextention.SLF"},
                {"filewithgoodextention.slf"},
        });
    }

    @Test
    public void paramTest() {
        LogAnalyzer analyzer = new LogAnalyzer();

        boolean result = analyzer.isValidLogFileName(fileName);

        Assert.assertTrue(result);
    }
}

package Chapter7;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class TimeLoggerTests {
    @Test
    public void settingSystemTime_Always_ChangesTime() {
        SystemTime.set(new Date(Long.parseLong("946684800000")));
        String output = TimeLogger.createMessage("a");
        Assert.assertTrue(output.contains("01.01.2000"));
    }

    @After
    public void afterEachTest(){
        SystemTime.reset();
    }
}

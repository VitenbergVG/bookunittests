package Chapter7;

import org.junit.After;
import org.junit.Test;

public class ConfigurationManagerTests {
    @Test
    public void analyze_EmptyFile_ThrowsException(){
        ConfigurationManager cm = new ConfigurationManager();
        cm.isConfigured("something");
        //Остальная часть теста
    }

    @After
    public void after()
    {
        // Необходимо сбрасывать статический ресурс между тестами
        LoggingFacility.logger = null;
    }
}

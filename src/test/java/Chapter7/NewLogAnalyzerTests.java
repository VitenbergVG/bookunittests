package Chapter7;

import org.junit.Test;

public class NewLogAnalyzerTests extends BaseTestsClass{
    @Test
    public void analyze_EmptyFile_ThrowsException(){
        fakeTheLogger();

        LogAnalyzer logan = new LogAnalyzer();
        logan.analyze("myemptyfile.txt");
        //Остальная часть теста
    }
}

package Chapter1;

import org.junit.Test;

public class SimpleParserTests {
    @Test
    public void TestReturnsZeroWhenEmptyString(){
        try{
            SimpleParser p = new SimpleParser();
            int result = p.parseAndSum("");
            if(result != 0){
                System.out.println("parseAndSum должен вернуть 0 для пустой строки");
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
    }
}

package Chapter5;

public class FakeLogger implements ILogger {//Поддельный регистратор(в роли заглушки)
    public Exception willThrow = null;
    public String loggerGotMessage = null;

    public void logError(String message) throws Exception {
        loggerGotMessage = message;
        if (willThrow != null) {
            throw willThrow;
        }
    }
}

package Chapter3;

public interface IExtensionManager {
    boolean isValid(String fileName);
}

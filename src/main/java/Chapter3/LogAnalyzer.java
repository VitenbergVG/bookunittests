package Chapter3;

import org.apache.commons.io.FilenameUtils;

public class LogAnalyzer {
    private IExtensionManager manager;

    public LogAnalyzer() {
        manager = ExtensionManagerFactory.create();
    }

    public boolean isValidLogFileName(String fileName) {
        return manager.isValid(fileName) && FilenameUtils.getBaseName(fileName).length() > 5;
    }
}

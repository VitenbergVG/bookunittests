package Chapter3;

import Chapter3.FakeExtensionManager;
import Chapter3.IExtensionManager;

public class LogAnalyzerUsingFactoryMethod {
    public boolean isValidLogFileName(String fileName){
        return getManager().isValid(fileName);
    }

    protected IExtensionManager getManager(){
        return new FakeExtensionManager();
    }
}

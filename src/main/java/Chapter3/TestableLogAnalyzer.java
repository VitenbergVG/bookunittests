package Chapter3;

import Chapter3.IExtensionManager;
import Chapter3.LogAnalyzerUsingFactoryMethod;

public class TestableLogAnalyzer extends LogAnalyzerUsingFactoryMethod {
    public TestableLogAnalyzer(IExtensionManager mgr){
        manager = mgr;
    }

    public IExtensionManager manager;

    @Override
    protected IExtensionManager getManager(){
        return manager;
    }
}

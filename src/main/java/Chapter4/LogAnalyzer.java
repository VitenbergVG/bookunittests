package Chapter4;

import Chapter3.ExtensionManagerFactory;
import Chapter3.IExtensionManager;


public class LogAnalyzer {

    private IExtensionManager manager;
    private IWebService service;
    private IEmailService email;

    public LogAnalyzer() {
        manager = ExtensionManagerFactory.create();
    }

    public LogAnalyzer(IWebService service, IEmailService email) {
        this.email = email;
        this.service = service;
    }


    public void analyze(String fileName) {
        if (fileName.length() < 8) {
            try {
                service.logError("Слишком короткое имя файла: " + fileName);
            } catch (Exception e) {
                email.sendMail("someone@someone.ru", "can't log", e.getMessage());
            }

        }
    }
}
package Chapter4;

public interface IWebService {
    void logError(String message) throws Exception;
}

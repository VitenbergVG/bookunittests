package Chapter4;

import Chapter4.IEmailService;

public class FakeEmailService implements IEmailService {
    public String to;
    public String subject;
    public String body;

    public void sendMail(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }
}

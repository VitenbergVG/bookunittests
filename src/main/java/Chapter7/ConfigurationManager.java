package Chapter7;

//Этот класс тоже использует LoggingFacility
public class ConfigurationManager {
    public boolean isConfigured(String configName)
    {
        LoggingFacility.log("Проверяется " + configName);
        return true; //Для демонстрации
    }
}

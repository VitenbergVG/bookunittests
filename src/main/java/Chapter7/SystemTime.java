package Chapter7;

import java.util.Date;

public class SystemTime {
    private static Date date;

    public static void set(Date custom) {
        date = custom;
    }

    public static void reset() {
        date = new Date(Long.MIN_VALUE);
    }

    public static Date now() {
        if (date != new Date(Long.MIN_VALUE)) {
            return date;
        }
        return new Date();
    }


}

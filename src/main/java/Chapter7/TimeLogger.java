package Chapter7;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeLogger {
//    public static String createMessage(String info) {
//        return new Date().toString() + " " + info;
//    }

    public static String createMessage(String info) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(SystemTime.now()) + " " + info;
    }
}
